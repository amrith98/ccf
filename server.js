var express = require('express');
var app = express();
var bodyparser = require('body-parser');
var mongoose = require('mongoose');
var server_port = 2605;
var server_ip_address = 'localhost';
var nodemailer = require("nodemailer");
var bcrypt = require('bcryptjs');
var jwt = require('jwt-simple');
var JWT_SECRET="youcanthackthis";
var baseurl='http://104.245.33.9:'+server_port;
var JWT_SECRET = "youcanthackthis";
var baseurl = 'http://104.245.33.9:' + server_port;
var nev = require('email-verification')(mongoose);
// var transporter = nodemailer.createTransport({
//     service: "gmail",
//     auth: {
//         user: "abc@gmail.com",
//         pass: "pass"
//     }
// });
app.use(express.static('public'));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/ccf');
var db = mongoose.connection; //DB Driver
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('connected to db');
});
var Schema = mongoose.Schema;
var Entryschema = new Schema({
    name: { type: String, required: true },
    semester: { type: Number, required: true },
    branch: { type: String, required: true },
    org: { type: String, required: true },
    date: { type: String, required: true },
    email: { type: String, required: true },
    phone: { type: Number, required: true },
    desc: { type: String }
});
var TempDat = new Schema({
    name: { type: String, required: true },
    semester: { type: String, required: true },
    branch: { type: String, required: true },
    org: { type: String, required: true },
    date: { type: String, required: true },
    email: { type: String, required: true },
    phone: { type: String, required: true },
    desc: { type: String },
    GENERATED_VERIFYING_URL: String
});
var AdminSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true }
});
var AdminTable = mongoose.model('admins', AdminSchema);
var TempUser = mongoose.model('TempUser', TempDat);
var Entry = mongoose.model('Entry', Entryschema);
module.exports = Entry;
app.get('/d', function(req, res, next) { //On Loading the site..Transfer Data to controller
    Entry.find(function(err, data) {
        if (err) return console.error(err);
        //console.log('db ' + data);
        res.send(data);
    }).sort({ date: 1 });
});
app.get('/verify/:id', function(req, res, next) { //For Confirming The Booking
    nev.confirmTempUser(req.params.id, function(err, user) {
        console.log('user confirm mail' + user);
        console.log('url ' + req.params.id);
        if (err) console.log('confirm ' + err);
        if (user) {
            console.log('confirm mail' + user);
            // optional
            nev.sendConfirmationEmail(user.email, function(err, info) {
                console.log('confirmation info' + info);
                Entry.findOneAndUpdate({ email: user.email }, { email: '*' + user.email }, { new: true }, function(err, mailmodobj) {
                    console.log('mail mod ' + mailmodobj);
                });
                // redirect to their profile...
            });
        }
        // user's data probably expired...
        else console.log('else');
        // redirect to sign-up
    });
    res.send(req.params.id);
});
app.post('/submit', function(req, res, next) { //Submitting an Event
    var newentry = new Entry({
        name: req.body.name,
        semester: req.body.semester,
        branch: req.body.branch,
        org: req.body.org,
        date: req.body.date,
        email: req.body.email,
        phone: req.body.phone,
        desc: req.body.desc
    });
    console.log(newentry);
    Entry.count({ date: req.body.date }, function(err, entry) { //
        console.log('entry ' + entry);
        if (entry == 0) {
            nev.configure({
                    verificationURL: baseurl + '/verify/${URL}',
                    persistentUserModel: Entry,
                    tempUserModel: TempUser,
                    tempUserCollection: 'tempusers',
                    transportOptions: {
                        service: 'Gmail',
                        auth: {
                            user: 'amrithdev98@gmail.com',
                            pass: 'developerworks'
                        }
                    },
                    verifyMailOptions: {
                        from: 'New Booking @ CCF <amrithdev98@gmail.com>',
                        subject: 'Please confirm the entry',
                        html: 'New Booking <br>' + 'Name: ' + newentry.name + '<br> Class: S' + newentry.semester + ' ' + newentry.branch + '<br> Organization: ' + newentry.org + '<br> date: ' + newentry.date + '<br> Phone Number: ' + newentry.phone + '<br>Click the following link to confirm event booking:</p><p>${URL}</p>',
                        text: 'Please confirm the entry by clicking the following URL: ${URL}'
                    },
                    confirmMailOptions: {
                        from: 'CCF CET<amrithdev98@gmail.com>',
                        subject: 'Successfully verified!',
                        html: 'Your Booking for ccf on ' + newentry.date + ' has been approved for the ' + newentry.org + ' event',
                        text: 'Your Booking has been successfully verified.'
                    },
                },
                function(error, options) {
                    if (err)
                        console.log('nev error ' + error);
                });
            nev.createTempUser(newentry, function(err, existingPersistentUser, newTempUser) {
                // some sort of error
                if (err) console.log(err);
                // user already exists in persistent collection...
                if (existingPersistentUser) {
                    console.log('existingPersistentUser ' + existingPersistentUser);
                }
                console.log('newTempUser ' + newTempUser);
                // handle user's existence... violently.
                // a new User
                //MULTIPLE USER PENDING ISSUE IS DUE TO EMAIL CONFLICT ... FINDING A PROPER FIX
                //Fixed with a *
                if (newTempUser) {
                    var URL = newTempUser[nev.options.URLFieldName];
                    nev.sendVerificationEmail('amrithdev98@gmail.com', URL, function(err, info) {
                        if (err) console.log(err);
                        else {
                            console.log('Ver info: ' + info);
                            res.send();
                        }
                        // handle error...
                        // flash message of successfully
                    });
                    // user already exists in temporary collection...
                } else {
                    console.log('new temp user failed');
                    res.send('email_exist');
                }
            });
        } else {
            console.log('Data Exists');
            res.send('fail');
        }
    });
});
app.get('/pend', function(req, res, next) { //On Loading the site..Transfer PENDING Data to controller
    TempUser.find(function(err, data) {
        if (err) return console.error(err);
        res.send(data);
    }).sort({ date: 1 });
});
app.post('/admin/login', function(req, res) {
    if (req.body != null)
        AdminTable.findOne({ email: req.body.email }, function(err, admin) {
            bcrypt.compare(req.body.password, admin.password, function(err, result) {
                // result == true 
                if (result) {
                    var token = jwt.encode(admin, JWT_SECRET);
                    return res.json({ token: token }); //generate token instead
                } else {
                    return res.status(400).send();
                }

            });
        });
});
app.post('/admin/signup', function(req, res) {
    bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(req.body.password, salt, function(err, hash) {
            if (err) console.log(err);
            // Store hash in your password DB. 
            var newadmin = new AdminTable({
                name: req.body.name,
                email: req.body.email,
                password: hash
            });
            newadmin.save(function(error) {
                if (error) throw error;
                console.log('admin added');
            });
            res.send();
        });
    });
});
app.post('/admin', function(req, res) {
    console.log(req.body);
    var newentry = new Entry({
        name: req.body.name,
        semester: req.body.semester,
        branch: req.body.branch,
        org: req.body.org,
        date: req.body.date,
        email: req.body.email,
        phone: req.body.phone,
        desc: req.body.desc
    });
    newentry.save(function(err) {
        if (err) throw err;
        console.log('User saved successfully!');
    });
    res.send();
});
app.delete('/admin/:id', function(req, res) { //Remove Entry from Table
    var id = req.params.id;
    console.log(id);
    Entry.find({ _id: id }).remove().exec();
    console.log('user removed');
});
app.delete('/pending/:id', function(req, res) { //Remove Pending Request
    var id = req.params.id;
    console.log(id);
    TempUser.find({ _id: id }).remove().exec();
    console.log('user removed');
});
app.get('/admin/:id', function(req, res) {
    var id = req.params.id;
    console.log(id);
    Entry.findById(id, function(err, myDocument) {
        console.log('mydoc' + myDocument);
        res.json(myDocument);
    });
});
app.put('/admin/:id', function(req, res) {
    var id = req.params.id;
    console.log(req.body.name);
    db.contactlist.findAndModify({
        query: { _id: mongojs.ObjectId(id) },
        update: { $set: { name: req.body.name, email: req.body.email, phone: req.body.phone } },
        new: true
    }, function(err, doc) {
        res.json(doc);
    });
});
app.listen(server_port);
console.log('server up!');
